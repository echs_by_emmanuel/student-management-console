<?php

class Students{

    function __construct() {
        if(!defined("STDIN")) {
            define("STDIN", fopen('php://stdin','rb'));
            }
      }

      function clearScreen(){
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            system('cls');
        } else {
            system('clear');
        }
      }
      function mainMenu(){
        
        $this->clearScreen();
        loop: 
          echo PHP_EOL;echo PHP_EOL; echo PHP_EOL;
          echo "-------------------Main Menu---------------------".PHP_EOL;
          echo "1. New Student ".PHP_EOL;
          echo "2. Search Student ".PHP_EOL;
          echo "3. View All Students ".PHP_EOL;
          echo PHP_EOL."Option :"; 
          $option = fread(STDIN, 80);

          if($option==1){
            $this->addStudent();
          }else if($option==2){
            $this->searchStudent();
          }else if($option==3){
            $this->getAllStudents();
          }else{
            $this->clearScreen();
            echo "You have entered an invalid key".PHP_EOL;
            goto loop;
          }
      }
      function deleteStudent($id){
        unlink($this->folder($id));
        if(count(glob("students/".substr(preg_replace("/\s+/", "",$id), 0, 2)."/*.json"))==0){
            rmdir("students/".substr(preg_replace("/\s+/", "",$id), 0, 2));
        }else{

        }
        echo PHP_EOL;
     
        echo "Student have been deleted successfully".PHP_EOL;
    
        echo PHP_EOL;
        echo "Enter any key to return to main menu: ";
        $value = fread(STDIN, 80);
        $this->mainMenu();

      }
      function searchStudent(){
          loop:
        echo PHP_EOL;
        echo "Enter Student ID: ";
        $std_id = fread(STDIN, 7);

        $isStudentId=$this->checkstudentId($std_id);
        if($isStudentId==true){
            $data = json_decode(file_get_contents($this->folder($std_id)),true);
            echo "Id\t\tName\t\tSurname\t\tAge\t\tCurriculum". PHP_EOL;
            echo $data['id']."\t\t".$data['name']."\t\t".$data['surname']."\t\t".$data['age']."\t\t".$data['curriculum']. PHP_EOL;
            echo PHP_EOL; 
            echo "1. Update".PHP_EOL;
            echo "2. Delete".PHP_EOL;
            echo "3. Main Menu".PHP_EOL;
            echo PHP_EOL."Option :"; 
            $option = fread(STDIN, 80); 

            if($option==1){
                $this->updateStudent($std_id);
            }else if($option==2){
                    $this->deleteStudent($std_id);
            }else if($option==3){
                    $this->mainMenu();
            }else{

            } 
        }else{
            echo PHP_EOL."You Have Entered Invalid Student Id Number";
            goto loop; 
        }
        
      }
      function updateStudent($id){
          $this->clearScreen();
          loop:
       
    
        $name=null;
        $surname=null;
        $age=null;
        $curriculum=null;
    
        
        $data = json_decode(file_get_contents($this->folder($id)),true);
        $this->clearScreen();
        echo "Id\t\tName\t\tSurname\t\tAge\t\tCurriculum". PHP_EOL;
        echo $data['id']."\t\t".$data['name']."\t\t".$data['surname']."\t\t".$data['age']."\t\t".$data['curriculum']. PHP_EOL;
        
        $name=$data['name'];
        $surname=$data['surname'];
        $age=$data['age'];
        $curriculum=$data['curriculum'];
          
        echo PHP_EOL."===================Update Student===================".PHP_EOL.PHP_EOL;

       
        
        
       
        loop_name:

            echo "Name: ".$name.PHP_EOL;
            echo "1. Use current Name".PHP_EOL;
            echo "2. Enter New Name".PHP_EOL;
            echo "Option :"; $option = fread(STDIN, 80);

            if($option==1){
               
            }else if($option==2){
                echo "Enter name :"; $name = fread(STDIN, 80);
                if(strlen($name)<3){
                    echo PHP_EOL."Please valid Name".PHP_EOL;
                    goto loop_name;
                }else{
                    if(is_numeric(preg_replace("/\s+/", "",$name))){
                        echo PHP_EOL."Please valid Name".PHP_EOL;
                        goto loop_name;
                    }
                }
               
            }else{
                echo "Enter the correct option".PHP_EOL;
                goto loop_name;
            }
            
            loop_surname:
            echo PHP_EOL.PHP_EOL."Surname: ".$surname.PHP_EOL;
            echo "1. Use current surname".PHP_EOL;
            echo "2. Enter New surname".PHP_EOL;
            echo "Option :"; $option = fread(STDIN, 80);

            if($option==1){
               
            }else if($option==2){
                echo "Enter surname :"; $surname = fread(STDIN, 80);
                if(strlen($surname)<3){
                    echo PHP_EOL."Please valid Surname".PHP_EOL;
                    goto loop_surname;
                }else{
                    if(is_numeric(preg_replace("/\s+/", "",$surname))){
                        echo PHP_EOL."Please valid Surname".PHP_EOL;
                        goto loop_surname;
                    }
                }
               
            }else{
                echo "Enter the correct option".PHP_EOL;
                goto loop_surname;
            }
            


            loop_age:
            echo PHP_EOL.PHP_EOL."Age: ".$age.PHP_EOL;
            echo "1. Use current age".PHP_EOL;
            echo "2. Enter New age".PHP_EOL;
            echo "Option :"; $option = fread(STDIN, 80);

            if($option==1){
               
            }else if($option==2){
                echo "Enter age :"; $age = fread(STDIN, 2);
                if(is_numeric(preg_replace("/\s+/", "",$age))){
                    
                }else{
                    echo PHP_EOL."Please enter valid age".PHP_EOL;
                    goto loop_age;
                }
               
            }else{
                echo "Enter the correct option".PHP_EOL;
                goto loop_age;
            }
            

           
            
         loop_curriculum:
         echo PHP_EOL.PHP_EOL."Curriculum: ".$curriculum.PHP_EOL;
         echo "1. Use current curriculum".PHP_EOL;
         echo "2. Enter New curriculum".PHP_EOL;
         echo "Option :"; $option = fread(STDIN, 80);

         if($option==1){
            
         }else if($option==2){
            echo "Enter curriculum :"; $curriculum = fread(STDIN, 80);
            if(strlen($curriculum)<3){
                echo PHP_EOL."Please valid curriculum".PHP_EOL;
                goto loop_curriculum;
            }else{
                if(is_numeric(preg_replace("/\s+/", "",$curriculum))){
                    echo PHP_EOL."Please valid curriculum".PHP_EOL;
                    goto loop_curriculum;
                }
            }
            
         }else{
             echo "Enter the correct option".PHP_EOL;
             goto loop_curriculum;
         }
         
        
         
        
        $array = array(
            'id'     => $id,
            'name'   => $name,
            'surname' => $surname,
            'age' => $age,
            'curriculum' => $curriculum
        );

       
        $jsonfile = str_replace('\n','',json_encode($array, JSON_PRETTY_PRINT));
            // save 
            unlink($this->folder($id));
            file_put_contents($this->folder($id), $jsonfile);
           
            $this->clearScreen();
            echo PHP_EOL."Student Successfully Updated".PHP_EOL;
            
            $data = json_decode(file_get_contents($this->folder($id)),true);

          
            echo "Id\t\tName\t\tSurname\t\tAge\t\tCurriculum". PHP_EOL;
            echo $data['id']."\t\t".$data['name']."\t\t".$data['surname']."\t\t".$data['age']."\t\t".$data['curriculum']. PHP_EOL;
                   
             

            echo PHP_EOL;
            echo "Enter any key to return to main menu: ";
            $value = fread(STDIN, 80);
            $this->mainMenu();

      }
      function checkstudentId($id){
       
        $res = false;
        if(file_exists('students/'.substr(preg_replace("/\s+/", "",$id), 0, 2).'/'.$id.'.json')){
           $res = true;
        }
        return $res;
      }
      function folder($id){
       
        if(!is_dir('students/'.substr(preg_replace("/\s+/", "",$id), 0, 2))){
            mkdir('students/'.substr(preg_replace("/\s+/", "",$id), 0, 2), 0777, true);
        }
        return 'students/'.substr(preg_replace("/\s+/", "",$id), 0, 2).'/'.preg_replace("/\s+/", "",$id).'.json';
      }
      function addStudent(){
            $this->clearScreen();
            $id=null;
            $name=null;
            $surname=null;
            $age=null;
            $curriculum=null;
            
            loop_id:
            echo "Enter id :"; $id = fread(STDIN, 7);
           
            if(is_numeric($id) && strlen($id)===7){//valid input, therefore validate
                 
              if($this->checkstudentId($id)===true){
                echo PHP_EOL."Student id already being used, please try different student no".PHP_EOL;
                goto loop_id;
              }else{

              }
            }else{//invalid input
                loop_invalid:
                echo PHP_EOL."Please  enter valid student id".PHP_EOL;
                goto loop_id;
                
            }
            loop_name:
            echo "Enter name :"; $name = fread(STDIN, 80);
            if(strlen($name)<3){
                echo PHP_EOL."Please valid Name".PHP_EOL;
                goto loop_name;
            }else{
                if(is_numeric(preg_replace("/\s+/", "",$name))){
                    echo PHP_EOL."Please valid Name".PHP_EOL;
                    goto loop_name;
                }
            }
           
            loop_surname:
            echo "Enter surname :"; $surname = fread(STDIN, 80);
            if(strlen($surname)<3){
                echo PHP_EOL."Please valid Surname".PHP_EOL;
                goto loop_surname;
            }else{
                if(is_numeric(preg_replace("/\s+/", "",$surname))){
                    echo PHP_EOL."Please valid Surname".PHP_EOL;
                    goto loop_surname;
                }
            }

            loop_age:
            echo "Enter age :"; $age = fread(STDIN, 2);
            if(is_numeric(preg_replace("/\s+/", "",$age))){
                   
            }else{
                echo PHP_EOL."Please enter valid age".PHP_EOL;
                goto loop_age;
            }
            
         loop_curriculum:
         echo "Enter curriculum :"; $curriculum = fread(STDIN, 80);
            if(strlen($curriculum)<3){
                echo PHP_EOL."Please valid curriculum".PHP_EOL;
                goto loop_curriculum;
            }else{
                if(is_numeric(preg_replace("/\s+/", "",$curriculum))){
                    echo PHP_EOL."Please valid curriculum".PHP_EOL;
                    goto loop_curriculum;
                }
            }
            $data = array(
                'id'     => $id,
                'name'   => $name,
                'surname' => $surname,
                'age' => $age,
                'curriculum' => $curriculum
            );

            
            $jsonfile = str_replace('\n','',json_encode($data, JSON_PRETTY_PRINT));
            // save ss
            
            file_put_contents($this->folder($id), $jsonfile);


            //disply added student

            $this->clearScreen();
            echo PHP_EOL."Student Successfully Added".PHP_EOL;
            
            $data = json_decode(file_get_contents($this->folder($id)),true);

           
                   
            echo "Id\t\tName\t\tSurname\t\tAge\t\tCurriculum". PHP_EOL;
            echo $data['id']."\t\t".$data['name']."\t\t".$data['surname']."\t\t".$data['age']."\t\t".$data['curriculum']. PHP_EOL;
           
            echo PHP_EOL;
            echo "Enter any key to return to main menu: ";
            $value = fread(STDIN, 80);
            $this->mainMenu();

      }
      function getAllStudents(){
       $this->clearScreen();
       allStudentMenu:
       $jsonfile = json_decode(file_get_contents('students.json'),true);
        
       echo "Id\t\tName\t\tSurname\t\tAge\t\tCurriculum". PHP_EOL;
       $dirs = array_filter(glob('students/*'), 'is_dir');
        
       foreach($dirs as $folder){
           foreach(glob($folder."/*.json") as $files){
            $data = json_decode(file_get_contents($files),true);
            echo $data['id']."\t\t".$data['name']."\t\t".$data['surname']."\t\t".$data['age']."\t\t".$data['curriculum']. PHP_EOL;
           }
       }
    
       
       
       echo PHP_EOL;echo PHP_EOL; 
       echo "1. Select Student".PHP_EOL;
       echo "2. Main Menu ".PHP_EOL;
       echo PHP_EOL."Option :"; 
       $id = fread(STDIN, 80);
      
       if($id==1){
           $this->searchStudent();
       }else if($id==2){
            $this->mainMenu();
       }else{//Inavlid Option
            $this->clearScreen(); 
            echo "You have enterd inavid key".PHP_EOL;
            goto allStudentMenu;
       }

      }
}

$students = new Students();
// $students->folder(7654321);
$students->mainMenu();




?>